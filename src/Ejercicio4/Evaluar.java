/*
 * NOMBRE PROGRAMA:FRASE PALINDROMA
 * AUTOR:Andres Felipe Wilches Torres
 * FECHA:21/03/18
 */
package Ejercicio4;
import java.text.Normalizer;
import java.util.regex.Pattern;
public class Evaluar {

	private boolean pal;
	public Evaluar(String frase){
		int letra=0;
		String frasen = null;
		int espacio;
		int aux=0;
		
		//convertir a minuscula
		frasen=frase.toLowerCase();
		
		//quitar acentos
		String ConAcento="�����";
		String SinAcento="aeiou";
		int ejemplares=ConAcento.length();
		String frasec=frasen;
		for(int i=0;i<ejemplares;i++){
			frasec=frasec.replace(ConAcento.charAt(i), SinAcento.charAt(i));
		}
		//quitar espacios
		for(int j=0;j<frasec.length();j++){
			if(j==32){
				espacio=j;
				aux=j;
				while(aux==32){
					aux++;
					espacio=aux;
					aux=32;
				}
			}
		}
		/*//quitar signos de interrogacion
		String normalized = Normalizer.normalize(frasec, Normalizer.Form.NFD);
	    
		// Nos quedamos �nicamente con los caracteres ASCII
	    Pattern pattern = Pattern.compile("\\p{ASCII}+");
	      String frasef=pattern.matcher(normalized).replaceFirst("");
	    System.out.print("abc"+frasef);
	       *///evaluar si es palindroma
			for(letra=0;letra<frasec.length();letra++){
				if(frasec.charAt(letra)!= frasec.charAt(frasec.length()-letra-1)){
					pal=false;
				}
				else{
					pal=true;
				}

			}
		}	
	public void setPal(boolean pal) {
		this.pal = pal;
	}
	public boolean isPal() {
		return pal;
	}
}
