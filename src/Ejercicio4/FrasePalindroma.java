/*
 * NOMBRE PROGRAMA:FRASE PALINDROMA
 * AUTOR:Andres Felipe Wilches Torres
 * FECHA:21/03/18
 */
package Ejercicio4;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class FrasePalindroma {
	public static void main(String Arg[ ]) throws IOException{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		int pregunta=0;
		do{
			String frase=null;
			//Obtener la frase
			System.out.print("Digite la frase: ");
			frase=in.readLine();
			//evaluar si es palindroma
			Evaluar palindroma=new Evaluar(frase);
			//imprimir respuesta
			if(palindroma.isPal()==true){
				System.out.print("La frase "+frase+" es palindroma");
			}
			else{
				System.out.print("La frase "+frase+" no es palindroma");
			}
			System.out.println("�Desea ejecutar el programa de nuevo?(si=1");
		}while(pregunta==1);
		if(pregunta!=1)
		{
			System.out.println("Gracias por usar el programa.");
		}	
	}
}
