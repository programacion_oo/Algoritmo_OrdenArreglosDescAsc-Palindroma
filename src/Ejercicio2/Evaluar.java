/*
 * NOMBRE PROGRAMA:PALABRA PALINDROMA
 * AUTOR:Andres Felipe Wilches Torres
 * FECHA:21/03/18
 */
package Ejercicio2;

public class Evaluar {
	private boolean pal;
	public Evaluar(String palabra){
		int letra=0;
		String palabran = null;
		//pasar a minuscula
		palabran=palabra.toLowerCase();
		//tomar letras a las que se quiere quitar el acento
		String ConAcento="�����";
		String SinAcento="aeiou";
		int ejemplares=ConAcento.length();
		String palabrac=palabran;
		//reemplazar letras con acento
		for(int i=0;i<ejemplares;i++){
			palabrac=palabrac.replace(ConAcento.charAt(i), SinAcento.charAt(i));
		}
		//evaluar si es palindroma
		for(letra=0;letra<palabrac.length();letra++){
			if(palabrac.charAt(letra)!= palabrac.charAt(palabrac.length()-letra-1)){
				pal=false;
			}
			else{
				pal=true;
			}

		}
	}
	//getters y setters
	public boolean isPal() {
		return pal;
	}
	public void setPal(boolean pal) {
		this.pal = pal;
	}
}
