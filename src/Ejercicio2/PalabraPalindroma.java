
package Ejercicio2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class PalabraPalindroma {
	public static void main(String Arg[ ]) throws IOException{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		int pregunta=0;
		do{
			//digitar palabra
			String palabra=null;
			System.out.print("Digite la palabra: ");
			palabra=in.readLine();
			//evaluar si es palindroma
			Evaluar palindroma=new Evaluar(palabra);
			//imprimir respuesta
			if(palindroma.isPal()==true){
				System.out.println("La palabra ''"+palabra+"'' es palindroma");
			}
			else{
				System.out.println("La palabra ''"+palabra+"'' no es palindroma");
			}
			System.out.println("�Desea ejecutar el programa de nuevo?(si==1)");
			pregunta = Integer.parseInt(in.readLine());
		}while(pregunta==1);
		if(pregunta!=1)
		{
			System.out.println("Gracias por usar el programa");
		}	
	}
}
