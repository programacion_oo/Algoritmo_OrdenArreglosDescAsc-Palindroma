/*
 * NOMBRE PROGRAMA:ORDENAR ARREGLO
 * AUTOR:Andres Felipe Wilches Torres
 * FECHA:14/03/18
 */
package Ejercicio1;

import java.util.Scanner;

public class Descendente {

	private int numero[];

	public Descendente() {
		Scanner entrada=new Scanner(System.in);
		numero = new int[7];
		int contador=0;
		int con=0,ord=0;
		int aux=0;
		for(contador =0;contador<7;contador++){
			System.out.print("Digite el no."+(contador+1)+": ");
			numero[contador]=entrada.nextInt();
		}

		for(con=0;con<(6);con++){
			for(ord=0;ord<(6);ord++){
				if(numero[ord]<numero[ord+1]){
					aux=numero[ord];
					numero[ord]=numero[ord+1];
					numero[ord+1]=aux;
				}
			}
		}
	}

	public int[] getNumero() {
		return numero;
	}

	public void setNumero(int[] numero) {
		this.numero = numero;
	}
}


