/*
 * NOMBRE PROGRAMA:ORDENAR ARREGLO
 * AUTOR:Andres Felipe Wilches Torres
 * FECHA:14/03/18
 */
package Ejercicio1;
import java.util.Scanner;
public class Ascendente {
	
	private int numero[];
	
	public Ascendente (){
		Scanner entrada = new Scanner(System.in);
		int datos=7;
		numero = new int[datos];
		int contador=0;
		int con=0,ord=0;
		int aux=0;
		for(contador =0;contador<datos;contador++){
			System.out.print("Digite el no."+(contador+1)+": ");
			numero[contador]=entrada.nextInt();
		}

		for(con=0;con<(datos-1);con++){
			for(ord=0;ord<(datos-1);ord++){
				if(numero[ord]>numero[ord+1]){
					aux=numero[ord];
					numero[ord]=numero[ord+1];
					numero[ord+1]=aux;
				}
			}
		}
	}

	public int[] getNumero() {
		return numero;
	}

	public void setNumero(int[] numero) {
		this.numero = numero;
	}
}

