/*
 * NOMBRE PROGRAMA:ORDENAR 10 NUMEROS
 * AUTOR:Andres Felipe Wilches Torres
 * FECHA:21/03/18
 */
package Ejercicio3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Ordenar10numeros {

	public static void main(String Arg[ ]) throws IOException{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		int pregunta;
		int opcion;
		do{

			int datos = 10;
			System.out.println(" ORDENAR 10 NUMEROS Y UNO ADICIONAL\n");
			do{	
				//menu de opciones	
				System.out.println(" MENU DE OPCIONES \n");
				System.out.println(" OPCION 1: ORDENAR ASCENDENTEMENTE");
				System.out.println(" OPCION 2: ORDENAR DESCENDENTEMENTE");
				System.out.print("Ingrese una opcion : ");
				opcion = Integer.parseInt(in.readLine( ));

				//mostrar aviso de opcion invalida
				if(opcion>3 || opcion<1){
					System.out.println("SELECCIONAR UNA OPCION VALIDA");
				}
				//Retornar al menu de opciones si no se escribe una opcion correcta	
			}while(opcion>3 || opcion<1);


			switch(opcion){
			//orden ascendente
			case 1:
			{
				Ascendente orden =new Ascendente();
				//imprimir arrglo ordenado con 10 numeros
				System.out.print("Los numeros en orden ascendente son: ");
				for(int fil = 0;fil<datos;fil++){
					System.out.print(orden.getNumero()[fil]+" ");
				}
				//agregar dato 
				Agregar ascendente =new Agregar(orden.getNumero()); 
				//imprimir arreglo ordenado con 11 datos
				for(int fil = 0;fil<11;fil++){
					System.out.print(ascendente.getNumeromas()[fil]+" ");
				}
			}
			break;
			//orden descendente
			case 2:
			{
				Descendente orden1=new Descendente();
				//imprimir arrglo ordenado con 10 numeros
				System.out.print("Los numeros en orden descendente son: ");
				for(int fil = 0;fil<datos;fil++){
					System.out.print(orden1.getNumero()[fil]+" ");
				}
				//agregar dato 
				Agregar2 descendente =new Agregar2(orden1.getNumero()); 
				//imprimir arreglo ordenado con 11 datos
				for(int fil = 0;fil<11;fil++){
					System.out.print(descendente.getNumeromas()[fil]+" ");
				}
			}	
			break;
			}
			System.out.println("�Desea ejecutar el programa de nuevo?(si==1)");
			pregunta = Integer.parseInt(in.readLine());
		}while(pregunta==1);
		if(pregunta!=1)
		{
			System.out.println("Gracias por usar el programa");
		}	
	}
}


