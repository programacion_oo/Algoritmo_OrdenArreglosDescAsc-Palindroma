
package Ejercicio3;
import java.util.Scanner;
public class Ascendente {
	
	private int numero[];
	
	public Ascendente (){
		Scanner entrada=new Scanner(System.in);
		//declarar variables
		int datos=10;
		int contador=0;
		int con=0,ord=0;
		int aux=0;
		//declarar array
		numero = new int[datos];
		//obtener datos para ordenar
		for(contador =0;contador<datos;contador++){
			System.out.print("Digite el no."+(contador+1)+": ");
			numero[contador]=entrada.nextInt();
		}
		//ordenar metodo burbuja
		for(con=0;con<(datos-1);con++){
			for(ord=0;ord<(datos-1);ord++){
				if(numero[ord]>numero[ord+1]){
					aux=numero[ord];
					numero[ord]=numero[ord+1];
					numero[ord+1]=aux;
				}
			}
		}
	}
	//getters y setters
	public int[] getNumero() {
		return numero;
	}

	public void setNumero(int[] numero) {
		this.numero = numero;
	}
}

